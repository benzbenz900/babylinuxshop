import { Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'babylinuxshop';
  search = ''
  
  constructor() {

  }

  searchText(e: any){
    this.search = e.target.value
  }

  gotoPage(){
    window.location.href = `product.html?search=${this.search}`;
  }
 
}
