import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './page/index/index.component';
import { ProductComponent } from './page/product/product.component';
import { HowtoshopComponent } from './page/howtoshop/howtoshop.component';
import { PreordergroupComponent } from './page/preordergroup/preordergroup.component';
import { ContactComponent } from './page/contact/contact.component';
import { DetailComponent } from './page/detail/detail.component';


const routes: Routes = [
  { path: '', redirectTo: '/index.html', pathMatch: 'full' },
  { path: 'index.html', component: IndexComponent },
  { path: 'product.html', component: ProductComponent },
  { path: 'howtoshop.html', component: HowtoshopComponent },
  { path: 'preordergroup.html', component: PreordergroupComponent },
  { path: 'contact.html', component: ContactComponent },
  { path: 'detail.html', component: DetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
