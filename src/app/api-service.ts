import { Product, Category, Banner, HowToUse, ProductDetail, IndexContent, ProductRecommend } from './api-json';

export class ApiService {
    endpoint = "https://admin.babylinuxshop.com/api/"
    apikey = "babylinuxshop-7A8D77D228EDCE24505C022407"

    async getProductAll(filter: any = 0, start: number = 0, limit: number = 12) {
        try {
            if (filter == 0) {
                var response = await fetch(`${this.endpoint}product/all?start=${start}&limit=${limit}`, this.Header());
            } else {
                var response = await fetch(`${this.endpoint}product/all?start=${start}&limit=${limit}${filter}`, this.Header());
            }
            const result = await response.json();
            return <Product>result;
        }
        catch (error) {
            console.log('error', error);
        }
    }

    private Header(): RequestInit {
        var myHeaders = new Headers();
        myHeaders.append("x-api-key", this.apikey);
        return {
            method: 'GET',
            headers: myHeaders,
            cache: "force-cache"
        };
    }

    async getHowTo() {
        try {
            const response = await fetch(`${this.endpoint}how_to_use/detail?id=1`, this.Header());
            const result = await response.json();
            return <HowToUse>result;
        }
        catch (error) {
            console.log('error', error);
        }
    }

    async getBannerAll() {
        try {
            const response = await fetch(`${this.endpoint}banner/all`, this.Header());
            const result = await response.json();
            return <Banner>result;
        }
        catch (error) {
            console.log('error', error);
        }
    }

    async getCategory() {
        try {
            const response = await fetch(`${this.endpoint}product_category/all`, this.Header());
            const result = await response.json();
            return <Category>result;
        }
        catch (error) {
            console.log('error', error);
        }
    }

    async getDetail(id: any) {
        try {
            const response = await fetch(`${this.endpoint}product/detail?id=${id}`, this.Header());
            const result = await response.json();
            return <ProductDetail>result;
        }
        catch (error) {
            console.log('error', error);
        }
    }

    async getIndexContent() {
        try {
            const response = await fetch(`${this.endpoint}IndexContent/detail?id=1`, this.Header());
            const result = await response.json();
            return <IndexContent>result;
        }
        catch (error) {
            console.log('error', error);
        }
    }

    async getProductRecommend() {
        try {
            const response = await fetch(`${this.endpoint}recommend_product/all`, this.Header());
            const result = await response.json();
            return <ProductRecommend>result;
        }
        catch (error) {
            console.log('error', error);
        }
    }
}
