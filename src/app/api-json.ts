interface Product {
    status: boolean;
    message: string;
    data: {
        product: {
            id: string;
            name: string;
            price: string;
            image: string;
        }[];
    };
};
interface ProductDetail {
    status: boolean;
    message: string;
    data: {
        product: {
            id: string;
            category: string;
            name: string;
            price: string;
            image: string;
            description: string;
            image_more: string[];
            shopeeurl: string;
            lazadaurl: string;
            facebookurl: string;
        };
    };
};
interface HowToUse {
    status: boolean;
    message: string;
    data: {
        how_to_use: {
            id: string;
            name: string;
            how_to_shop: string;
            how_to_pre: string;
            how_to_preshop: string;
            how_to_refund: string;
        };
    };
};
interface Category {
    status: boolean;
    message: string;
    data: {
        product_category: {
            id_cat: string;
            name_cat: string;
        }[];
    };
    total: number;
}
interface Banner {
    status: boolean;
    message: string;
    data: {
        banner: {
            id: string;
            name: string;
            image: string;
            linkto: string;
            showdate: string;
            enddate: string;
        }[];
    };
    total: number;
}
interface IndexContent {
    status: boolean;
    message: string;
    data: {
        IndexContent: {
            id: string;
            image1: string;
            image2: string;
            title1: string;
            text1: string;
            link1: string;
            namelink1: string;
            image3: string;
            title2: string;
            text2: string;
            link2: string;
            namelink2: string;
            image4: string;
            title3: string;
            text3: string;
            link3: string;
            namelink3: string;
        };
    };
};
interface ProductRecommend {
    status: boolean;
    message: string;
    data: {
        recommend_product: {
            id_rec: string;
            id_product: string;
            image_custom: string;
            title_custom: string;
            text_custom: string;
        }[];
    };
    total: number;
};
export { Product, Category, Banner, HowToUse, ProductDetail, IndexContent,ProductRecommend }