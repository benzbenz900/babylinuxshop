import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreordergroupComponent } from './preordergroup.component';

describe('PreordergroupComponent', () => {
  let component: PreordergroupComponent;
  let fixture: ComponentFixture<PreordergroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreordergroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreordergroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
