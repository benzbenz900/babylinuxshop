import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api-service';
import { ActivatedRoute } from '@angular/router';
import { ProductDetail, Product } from 'src/app/api-json';
import slugify from 'slugify';
import { Title, Meta } from '@angular/platform-browser';
declare var $: any;
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  private api = new ApiService
  id: number;
  dataProductDetail: ProductDetail;
  dataProduct: Product;
  dataProduct2: Product;
  constructor(
    private route: ActivatedRoute,
    private titleService: Title,
    private metaTagService: Meta
  ) {

    this.route.queryParams.subscribe(params => {
      console.log(params['id'])
      if (params['id']) {
        this.loadProduct(params['id'])
      } else {
        this.loadProduct(0)
      }

    });

    this.api.getProductAll(0, 0, 4).then(e => {
      this.dataProduct = e
    })

    this.api.getProductAll(0, 4, 4).then(e => {
      this.dataProduct2 = e
    })

  }

  loadProduct(number: any) {
    console.log(number)
    this.api.getDetail(number).then(e => {
      this.dataProductDetail = e
      this.titleService.setTitle(`${this.dataProductDetail?.data?.product?.name} | Baby Linux Shop`);

      this.metaTagService.addTags([
        { name: 'keywords', content: 'เสื้อผ้าเด็กเล็ก,ของใช้เด็กตั้งแต่แรกเกิด,ผ้าห่มเด็ก,ชุดนอนเด็ก,ของเล่นเด็กเสริมพัฒนาการ,ผ้าอ้อมเด็ก,คอกกั้นเด็ก,คอกหัดเดิน' },
        { name: 'robots', content: 'index, follow' },
        { name: 'author', content: 'Dev By cii3.net' },
        { name: 'description', content: `${this.dataProductDetail?.data?.product?.name} | BABY LINUX SHOP คือร้านขายอุปกรณ์ของใช้แม่และเด็กออนไลน์ ที่นำเข้าและจำหน่ายสินค้าทั้งปลีกและส่ง` },
      ]);
    })
  }

  Slugify(r: string) {
    return slugify(r, {
      replacement: '-',
      remove: /[*+~.()'"!:@]/g,
      lower: true,
      strict: false,
    })
  }

  ngOnInit(): void {
  }

  changeCarousel(num: any): void {
    $('#productShowDetail').carousel(num)
  }

}
