import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api-service';
import { Product, Category, Banner } from 'src/app/api-json';
import { Router, ActivatedRoute } from '@angular/router';
import slugify from 'slugify';
import { Title, Meta } from '@angular/platform-browser';
declare var $: any;
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  private api: ApiService = new ApiService;
  dataProduct: Product;
  dataCategory: Category;
  filter: any
  categoryName: string = 'ทั้งหมด'
  bannerAll: Banner;
  filterIN: string = 'หมวดหมู่';
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private metaTagService: Meta
  ) {

    this.titleService.setTitle('สินค้าพรีออเดอร์โดย Baby Linux Shop สำหรับลูกค้าที่เคยพรีสินค้ากับเรา จะไม่มีการเก็บมัดจำ ลูกค้าใหม่จะขอเก็บมัดจำ 50% ของราคาสินค้า');

    this.metaTagService.addTags([
      { name: 'keywords', content: 'เสื้อผ้าเด็กเล็ก,ของใช้เด็กตั้งแต่แรกเกิด,ผ้าห่มเด็ก,ชุดนอนเด็ก,ของเล่นเด็กเสริมพัฒนาการ,ผ้าอ้อมเด็ก,คอกกั้นเด็ก,คอกหัดเดิน' },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'Dev By cii3.net' },
      { name: 'description', content: 'BABY LINUX SHOP คือร้านขายอุปกรณ์ของใช้แม่และเด็กออนไลน์ ที่นำเข้าและจำหน่ายสินค้าทั้งปลีกและส่ง สินค้าส่วนใหญ่ที่เราดีลมาขาย และรอการนำเข้า จะเป็นสินค้าขายปลีก สำหรับเด็กที่ราคาจับต้องได้ คัดคุณภาพด้วยการดีลกับโรงงานที่เราเคยซื้อขายด้วยโดยตรง' },
    ]);

    this.route.queryParams.subscribe(params => {
      if (params['search']) {
        this.filterIN = 'ค้นหา';
        this.categoryName = params['search']
        this.filter = `&filter=${params['search']}`
        this.loadProduct(this.filter)
      } else {
        if (params['categoryName']) {
          this.categoryName = params['categoryName']
        } else {
          this.categoryName = 'ทั้งหมด'
        }
        this.filterIN = 'หมวดหมู่';
        if (params['category']) {
          this.filter = `&field=category&filter=${params['category']}`
          this.loadProduct(this.filter)
        } else {
          this.loadProduct(0)
        }
      }

    });

    this.api.getCategory().then(e => {
      this.dataCategory = e
    })

    this.api.getBannerAll().then(e => {
      this.bannerAll = e
    })

  }

  ngOnInit(): void {

  }

  loadProduct(filter: any) {
    this.api.getProductAll(filter).then(e => {
      this.dataProduct = e
    })
  }

  changeCarousel(num: any): void {
    $('#bannerShowProduct').carousel(num)
  }

  Slugify(r: string) {
    return slugify(r, {
      replacement: '-',
      remove: /[*+~.()'"!:@]/g,
      lower: true,
      strict: false,
    })
  }

  isLinkActive(url: any): boolean {
    return this.router.url === url;
  }

}
