import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api-service';
import { Product, Banner, IndexContent, ProductRecommend } from 'src/app/api-json';
import slugify from 'slugify';
import { Title, Meta } from '@angular/platform-browser';
declare var $: any;
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  private api: ApiService = new ApiService;
  dataProduct: Product;
  dataProduct2: Product;
  bannerAll: Banner;
  indexContent: IndexContent;
  ProductRecommend: ProductRecommend;

  constructor(
    private titleService: Title,
    private metaTagService: Meta
  ) {

    this.titleService.setTitle('Baby Linux Shop เสื้อผ้าเด็กเล็ก ของใช้เด็กตั้งแต่แรกเกิด');

    this.metaTagService.addTags([
      { name: 'keywords', content: 'เสื้อผ้าเด็กเล็ก,ของใช้เด็กตั้งแต่แรกเกิด,ผ้าห่มเด็ก,ชุดนอนเด็ก,ของเล่นเด็กเสริมพัฒนาการ,ผ้าอ้อมเด็ก,คอกกั้นเด็ก,คอกหัดเดิน' },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'Dev By cii3.net' },
      { name: 'description',content:'BABY LINUX SHOP คือร้านขายอุปกรณ์ของใช้แม่และเด็กออนไลน์ ที่นำเข้าและจำหน่ายสินค้าทั้งปลีกและส่ง สินค้าส่วนใหญ่ที่เราดีลมาขาย และรอการนำเข้า จะเป็นสินค้าขายปลีก สำหรับเด็กที่ราคาจับต้องได้ คัดคุณภาพด้วยการดีลกับโรงงานที่เราเคยซื้อขายด้วยโดยตรง'},
    ]);

    this.api.getBannerAll().then(e => {
      this.bannerAll = e
    })

    this.api.getProductAll(0, 0, 4).then(e => {
      this.dataProduct = e
    })

    this.api.getProductAll(0, 4, 4).then(e => {
      this.dataProduct2 = e
    })

    this.api.getIndexContent().then(e => {
      this.indexContent = e
    })

    this.api.getProductRecommend().then(e => {
      this.ProductRecommend = e
    })

  }

  ngOnInit(): void {

  }

  Slugify(r: string) {
    return slugify(r, {
      replacement: '-',
      remove: /[*+~.()'"!:@]/g,
      lower: true,
      strict: false,
    })
  }

  changeCarousel(num: any): void {
    $('#bannerShowIndex').carousel(num)
  }

}
