import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowtoshopComponent } from './howtoshop.component';

describe('HowtoshopComponent', () => {
  let component: HowtoshopComponent;
  let fixture: ComponentFixture<HowtoshopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowtoshopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowtoshopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
