import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api-service';
import { HowToUse } from 'src/app/api-json';
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-howtoshop',
  templateUrl: './howtoshop.component.html',
  styleUrls: ['./howtoshop.component.css']
})
export class HowtoshopComponent implements OnInit {
  private api: ApiService = new ApiService;
  howtouse: HowToUse;
  constructor(
    private titleService: Title,
    private metaTagService: Meta
  ) {

    this.titleService.setTitle('วิธีสั่งชื้อกับ Baby Linux Shop');

    this.metaTagService.addTags([
      { name: 'keywords', content: 'เสื้อผ้าเด็กเล็ก,ของใช้เด็กตั้งแต่แรกเกิด,ผ้าห่มเด็ก,ชุดนอนเด็ก,ของเล่นเด็กเสริมพัฒนาการ,ผ้าอ้อมเด็ก,คอกกั้นเด็ก,คอกหัดเดิน' },
      { name: 'robots', content: 'index, follow' },
      { name: 'author', content: 'Dev By cii3.net' },
      { name: 'description',content:'BABY LINUX SHOP คือร้านขายอุปกรณ์ของใช้แม่และเด็กออนไลน์ ที่นำเข้าและจำหน่ายสินค้าทั้งปลีกและส่ง สินค้าส่วนใหญ่ที่เราดีลมาขาย และรอการนำเข้า จะเป็นสินค้าขายปลีก สำหรับเด็กที่ราคาจับต้องได้ คัดคุณภาพด้วยการดีลกับโรงงานที่เราเคยซื้อขายด้วยโดยตรง'},
    ]);
    this.api.getHowTo().then(e => {
      this.howtouse = e
    })
  }

  ngOnInit(): void {
  }

}
